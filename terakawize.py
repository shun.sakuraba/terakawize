#!/usr/bin/env python
# Copyright (C) 2012-2015 Shun Sakuraba
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version. See LICENSE file for details.

# Implementation of Terakawa, Kameda & Takada, JCC 32, 1228 (2011).

import sys
import os
import os.path
import shutil
import math
import collections
import copy

# TODO: set() for 2.3

# popen and others are used only for 2.3 compatibility
import warnings
warnings.simplefilter("ignore", DeprecationWarning)

import popen2
from optparse import OptionParser

parser = OptionParser()

# topology, mandatory
parser.add_option("-t", "--top", dest = "topology",
                  help = "Gromacs topology file (*.top)",
                  default = None)
parser.add_option("-O", "--output", dest = "output",
                  help = "output Gromacs topology file",
                  default = None)

# "#define"s.
parser.add_option("-D", dest = "defines", action="append",
                  help="Defined attributes",
                  default = [])
parser.add_option("-I", dest = "includes", action="append",
                  help="#include directive lookup paths",
                  default = [])
parser.add_option("--cpp", dest = "cpp",
                  help = "How to invoke cpp (1) command (default = cpp)",
                  default = "cpp")
parser.add_option("--no-auto-dir-search", dest = "auto_dir_search", action = "store_false",
                  help = "Stop default DIR searching.", 
                  default = True)

parser.add_option("-l", "--lambda", dest = "lam",
                  help = "Temp(low) / Temp(high) (default 0.5)",
                  type = "float",
                  default = 0.5)
parser.add_option("--suffix", dest = "suffix",
                  help = "Suffix to be added to scaled atoms",
                  type = "str",
                  default = "+")

# can be specified; otherwise specify interactively
parser.add_option("-s", "--solute", dest = "solute",
                  help = "Molecular type of solute molecule (comma separated for multiple choice)",
                  default = None)

parser.add_option("-v", "--verbose", dest = "verbose", action = "store_true",
                  help = "Increase verbosity (for debug)",
                  default = False)

(options, args) = parser.parse_args()

def log(x):
    if options.verbose:
        print >> sys.stderr, x

def warn_or_error(x, col):
    prefix = ""
    suffix = ""
    if os.isatty(sys.stderr.fileno()):
        prefix = "\x1b[" + str(col) + ";1m"
        suffix = "\x1b[0m"
    print >> sys.stderr, prefix + x + suffix

def warn(x):
    warn_or_error("Warning: " + x, 33)

def error(x):
    warn_or_error("Error: " + x, 31)
    sys.exit(1)

if options.topology == None:
    error("topology must be specified (try --help)")

def find_luck():
    candidates = ["luck", "luck_d", "g_luck", "g_luck_d"]
    for cmd in candidates:
        (stdin, stdout, stderr) = os.popen3(" ".join(["which", cmd]))
        # May block if PIPE bufsize is extremely small, but should be enough for which(1) output
        stdin.close()
        output = "".join(stdout.readlines())
        _errors = stderr.readlines()

        if output != "":
            return output
    return None

def where_gromacs():
    try:
        if os.environ.has_key("GMXDATA"):
            p1 = os.path.join(os.environ["GMXDATA"], "top")
            p2 = os.path.join(os.environ["GMXDATA"], "gromacs/top")
            ps = [p1, p2]
        else:
            output = find_luck()
            log("Found luck at \"%s\"" % output)
            if output == None:
                if options.includes == []:
                    warn("Could not find luck / g_luck (1). Default search path to (gromacs-root-path)/share/gromacs/top is not set; please specify with -I")
                return
            
            rp = os.path.realpath(output)
            bindir = os.path.dirname(rp)
            ps = [os.path.normpath(os.path.join(bindir, "../share/gromacs/top"))]

        for p in ps:
            if os.path.exists(p):
                log("Found gromacs topology path %s" % p)
                options.includes.append(p)

    except OSError:
        pass

class topology:
    def __init__(self):
        self.pairtype = {}
        self.nonbondparams = {}
        self.bondtype = {}
        self.angletype = {}
        self.constrainttype = {}
        self.dihedtype = {}
        self.cmaptype = {}
        self.atom2type = {}
        self.atom2anum = {}
        self.atom2charge = {}
        self.atom2mass = {}
        self.atom2sigma = {}
        self.atom2epsilon = {}
        self.moleculetypes = {}
        self.molecules = []
        self.system = []
        self.ljtype = 0
        self.moleculetopology = {}
        self.defaults = []

    def process_defaults(self, ls):
        self.defaults = ls
        if int(ls[0]) != 1:
            warn("Non-bond type is Buckingham potential, which is not supported")
        self.ljtype = int(ls[1])
        pat = self.ljtype - 1
        log("LJtype = %s" % (["C6/CN - geometric mean",
                              "arithmetic mean",
                              "geometric mean"][pat]))

    def process_atomtypes(self, ls):
        # OK, atomtypes in GROMACS is full of mess;
        # The problem is that two of 6 fields are OPTIONAL.
        # see push_at function src/kernel/toppush.c; it's OFFICIALLY full of mess!
        if len(ls) < 6:
            error("Error processing atomtypes section: too few elements")

        (have_bonded_type, have_atomic_number) = (None, None)

        log("Molecular structure: ")
        log(ls)

        if ls[3].isalpha() and len(ls[3]) == 1:
            # single character
            (have_bonded_type, have_atomic_number) = (False, False)
        elif ls[5].isalpha() and len(ls[5]) == 1:
            (have_bonded_type, have_atomic_number) = (True, True)
        elif ls[4].isalpha() and len(ls[4]) == 1:
            if ls[1][0].isalpha():
                (have_bonded_type, have_atomic_number) = (True, False)
            else:
                (have_bonded_type, have_atomic_number) = (False, True)
        
        if have_bonded_type == None or have_atomic_number == None:
            error("Invalid format of atomtypes")

        atomic_number = 0
        aname = ls[0]

        offset = 0
        if have_bonded_type:
            offset += 1
        if have_atomic_number:
            atomic_number = int(ls[offset + 1])
            offset += 1

        mass = float(ls[offset + 1])
        charge = float(ls[offset + 2])
        sigma = float(ls[offset + 4])
        eps = float(ls[offset + 5])

        self.atom2mass[aname] = mass
        self.atom2charge[aname] = charge
        self.atom2sigma[aname] = sigma
        self.atom2epsilon[aname] = eps
        self.atom2anum[aname] = atomic_number

    def process_pairtypes(self, ls):
        key = (ls[0], ls[1]) # a1, a2
        self.pairtype[key] = (int(ls[2]), float(ls[3]), float(ls[4]))
    
    def process_nonbondparams(self, ls):
        key = (ls[0], ls[1]) # a1, a2
        self.nonbondparams[key] = (int(ls[2]), float(ls[3]), float(ls[4]))

    def process_constrainttypes(self, ls):
        key = (ls[0], ls[1], int(ls[2])) # ai, aj, func
        self.constrainttype[key] = float(ls[3])

    def process_bondtypes(self, ls):
        key = (ls[0], ls[1], int(ls[2])) # ai, aj, func
        self.bondtype[key] = (float(ls[3]), float(ls[4]))

    def process_angletypes(self, ls):
        key = (ls[0], ls[1], ls[2], int(ls[3])) # ai, aj, ak, func
        self.angletype[key] = tuple([float(l) for l in ls[4:]])

    def process_dihedraltypes(self, ls):
        key = (ls[0], ls[1], ls[2], ls[3], int(ls[4])) # ai, aj, ak, al, func
        if key not in self.dihedtype:
            self.dihedtype[key] = []
        self.dihedtype[key].append(ls[5:])

    def process_cmaptypes(self, ls):
        key = (ls[0], ls[1], ls[2], ls[3], ls[4], int(ls[5])) # ai, aj, ak, al, am, func
        if key not in self.cmaptype:
            self.cmaptype[key] = []
        self.cmaptype[key].append(ls[6:])

    def process_atoms(self, mtype, ls):
        _anum = ls[0]
        atype = ls[1]
        resnum = int(ls[2])
        resname = ls[3]
        atom = ls[4]
        _chargegrp = ls[5]
        if len(ls) >= 7:
            charge = float(ls[6])
        else:
            charge = self.atom2charge[atype]
        if len(ls) >= 8:
            mass = float(ls[7])
        else:
            mass = self.atom2mass[atype]
        
        self.molecules[self.moleculetypes[mtype]].append((atype, atom, resnum, resname, charge, mass))

    def molecule(self, mtype, fh):
        log("Entering molecule section \"%s\"" % mtype)
        self.moleculetypes[mtype] = len(self.molecules)
        self.molecules.append([])
        self.moleculetopology[mtype] = collections.OrderedDict()

        state = None

        for l in fh:
            l = l.split(';')[0] # remove comments
            l = l.strip()
            if l == "":
                continue
            
            if l[0] == '[':
                state = l.split()[1]
                log("  Entering state %s inside %s" % (state, mtype))
                continue
            
            if l[0] == '#':
                # already parsed by cpp; indicating line nubers etc.
                continue
            
            ls = l.split()
            if state == "atoms":
                self.process_atoms(mtype, ls)
            elif state == "moleculetype":
                return self.molecule(ls[0], fh)
            elif state == "system":
                pass
            elif state == "molecules":
                groupname = ls[0]
                nmol = int(ls[1])
                if nmol != 0:
                    self.system.append((groupname, nmol))
                pass
            elif state == None:
                pass
            else:
                # Just copy & paste to info
                vals = l.split()
                if state not in self.moleculetopology[mtype]:
                    self.moleculetopology[mtype][state] = []
                self.moleculetopology[mtype][state].append(vals)
        pass

    def parse_top(self, fh):
        state = None
        for l in fh:
            l = l.split(';')[0] # remove comments
            l = l.strip()
            if l == "":
                continue
            
            if l[0] == '[':
                state = l.split()[1]
                continue
            
            if l[0] == '#':
                # already parsed by cpp; indicating line nubers etc.
                continue
            
            ls = l.split()
            if ls == []:
                continue
            if state == "defaults":
                self.process_defaults(ls)
            elif state == "nonbond_params":
                self.process_nonbondparams(ls)
            elif state == "atomtypes":
                self.process_atomtypes(ls)
            elif state == "bondtypes":
                self.process_bondtypes(ls)
            elif state == "pairtypes":
                self.process_pairtypes(ls)
            elif state == "constrainttypes":
                self.process_constrainttypes(ls)
            elif state == "angletypes":
                self.process_angletypes(ls)
            elif state == "dihedraltypes":
                self.process_dihedraltypes(ls)
            elif state == "cmaptypes":
                self.process_cmaptypes(ls)
            elif state == "moleculetype":
                return self.molecule(ls[0], fh)
            pass

if not os.path.exists(options.topology):
    print >> sys.stderr, "Error: topology file does not exist!"
    sys.exit(1)

if options.auto_dir_search:
    where_gromacs()
    options.includes.append(".")

includes = ["-I" + i for i in options.includes]
defines = ["-D" + d for d in options.defines]
cmd = [options.cpp, "--traditional-cpp"] + includes + defines + [options.topology]
log("Invoking command: " + repr(cmd))

ppw = popen2.Popen3(cmd)
ppw.tochild.close()

top = topology()
top.parse_top(ppw.fromchild)

ppw.fromchild.close()
retcode = ppw.wait()

if retcode != 0:
    warn("Warning: cpp reported an error on preprocessing. Perhaps you forgot to add gromacs default topology path (/usr/share/gromacs/top)?")

def get_topology_info():
    # make list of segment names
    mollist = [s for (s, _n) in top.system]

    # python 2.3 does not have set()
    if options.solute:
        solutes = options.solute.split(',')
    else:
        # interactive input
        print "Molecule types in topology file:",
        for s in mollist:
            print s,
        print
        l = raw_input("Which molecules are solutes? (For multiple choice please specify as comma-separated list) ").split(',')
        solutes = [x.strip() for x in l]
    return solutes

def find_bond_map(ti, tj, func):
    if (ti, tj, func) in top.bondtype:
        return top.bondtype[(ti, tj, func)]
    elif (tj, ti, func) in top.bondtype:
        return top.bondtype[(tj, ti, func)]
    else:
        print >> sys.stderr, "Uknown map: %s %s %d" % (ti, tj, func)

def find_angle_map(ti, tj, tk, func):
    if (ti, tj, tk, func) in top.angletype:
        return top.angletype[(ti, tj, tk, func)]
    elif (tk, tj, ti, func) in top.angletype:
        return top.angletype[(tk, tj, ti, func)]
    else:
        error("Uknown map: %s %s %s %d" % (ti, tj, tk, func))

# Find corresponding dihedrals. bit tricky.
def lookup_dihed(ts, func):
    [ti, tj, tk, tl] = ts
    if (ti, tj, tk, tl, func) in top.dihedtype:
        return top.dihedtype[(ti, tj, tk, tl, func)]
    elif (tl, tk, tj, ti, func) in top.dihedtype:
        return top.dihedtype[(tl, tk, tj, ti, func)]
    else:
        solution = []
        for (tim, tjm, tkm, tlm, funcm) in top.dihedtype:
            if func != funcm:
                continue
            if (((tim == "X" or ti == tim) and
                 (tjm == "X" or tj == tjm) and
                 (tkm == "X" or tk == tkm) and
                 (tlm == "X" or tl == tlm)) or
                ((tim == "X" or tl == tim) and
                 (tjm == "X" or tk == tjm) and
                 (tkm == "X" or tj == tkm) and
                 (tlm == "X" or ti == tlm))):
                # found solution
                solution.append([tim, tjm, tkm, tlm])
        if solution == []:
            error("Uknown dihedral mapping: %s %s %s %s %d" % (ti, tj, tk, tl, func))
        else:
            best = 5
            bestsol = []
            for s in solution:
                count = 0
                for m in s:
                    if m == "X":
                        count += 1
                if count < best:
                    bestsol = s
            [tim, tjm, tkm, tlm] = bestsol
            return top.dihedtype[(tim, tjm, tkm, tlm, func)]

topolfile = options.topology
writeto = options.output

atomtable = {}

solutes = get_topology_info()

fhout = open(writeto, "wt")

fhout.write("; Generated by:\n")
fhout.write("; %s\n" % " ".join(sys.argv))

scalefac = options.lam

# defaults section:
fhout.write("[ defaults ]\n")
fhout.write(" ".join(top.defaults) + "\n\n")

def usedatoms(solutes):
    ret = set()
    for s in solutes:
        atoms = top.molecules[top.moleculetypes[s]]
        for (atype, _atom, _resnr, _resname, _charge, _mass) in atoms:
            ret.add(atype)
    return list(ret)

# helper function: counts appearance of target in list, and returns 2^found combinations
import copy
def generate_combination(ls, target, suffix):
    ret = []
    counts = 0
    indices = []
    for li in range(len(ls)):
        if ls[li] in target:
            counts += 1
            indices.append(li)
    for i in range(1 << counts):
        entity = copy.copy(ls)
        nocc = 0
        for j in range(counts):
            if i & (1 << j) > 0:
                nocc += 1
                # not += to prevent destructive operation
                entity[indices[j]] = entity[indices[j]] + suffix 
        ret.append((nocc, entity))
    return ret

# for bonded interactions: since we scale all bonded atoms, all selected targets have additional entry
def generate_scaled(ls, target, suffix):
    ret = []
    for l in ls:
        if l in target:
            ret.append(l + suffix)
        else:
            ret.append(l)
    return ret

# atom types. Determine atoms to be rewritten.
fhout.write("[ atomtypes ]\n")
def print_atom(aname, suffix, scale):
    mass = top.atom2mass[aname]
    anum = top.atom2anum[aname]
    charge = top.atom2charge[aname] * math.sqrt(scale)
    sigma = top.atom2sigma[aname]
    eps = top.atom2epsilon[aname] * scale
    fhout.write("%-10s %5d %10.3f %8.4f %s %19.12f %19.12f\n" % 
                (aname + suffix, anum, mass,
                 charge, "A", 
                 sigma, eps))
usedmolecules = [m for (m, _n) in top.system] 
selectedatoms = set(usedatoms(usedmolecules))
for aname in sorted(selectedatoms):
    print_atom(aname, "", 1.0)
scaledatoms = set(usedatoms(solutes))
for aname in sorted(scaledatoms):
    print_atom(aname, options.suffix, scalefac)
fhout.write("\n")

# pairtypes
def print_nonbond2(dic):
    for key in sorted(dic.keys()):
        (a1, a2) = key
        if a1 not in selectedatoms or a2 not in selectedatoms:
            continue
        values = dic[key]
        combs = generate_combination([a1, a2], scaledatoms, options.suffix)
        for (occ, p) in combs:
            if a1 == a2 and p[1] < p[0]:
                continue
            fhout.write("%-8s %-8s %d %16f %16f\n" % 
                        (p[0], p[1], 
                         values[0], values[1], 
                         values[2] * scalefac ** (occ / 2.0)))
    fhout.write("\n")

if top.pairtype != {}:
    # pairtype
    fhout.write("[ pairtypes ]\n")
    print_nonbond2(top.pairtype)

# bondtypes, angletypes
def print_bondtypes(dic, n):
    for key in sorted(dic.keys()):
        selected = True
        for i in range(n):
            if key[i] not in selectedatoms:
                selected = False
                break
        if not selected:
            continue
        values = dic[key]
        keys = list(key)[0:n]
        func = key[n]
        combs = generate_combination(keys, scaledatoms, options.suffix)
        for (occ, p) in combs:
            if n == 2:
                if key[0] == key[1] and p[1] < p[0]:
                    continue
            elif n == 3:
                if key[0] == key[2] and p[2] < p[0]:
                    continue
            fhout.write("%s %d %s\n" % 
                        (" ".join("%-8s" % q for q in p),
                         func,
                         " ".join([str(x) for x in values])))
    fhout.write("\n")


if top.bondtype != {}:
    fhout.write("[ bondtypes ]\n")
    print_bondtypes(top.bondtype, 2)
#    for key in sorted(top.bondtype.keys()):
#        (a1, a2, func) = key
#        if a1 not in selectedatoms or a2 not in selectedatoms:
#            continue
#        values = top.bondtype[key]
#        fhout.write("%-8s %-8s %d %16f %16f\n" %
#                    (a1, a2, func,
#                     values[0], values[1]))
#    fhout.write("\n")

if top.constrainttype != {}:
    fhout.write("[ constrainttypes ]\n")
    for key in sorted(top.constrainttype.keys()):
        (a1, a2, func) = key
        if a1 not in selectedatoms or a2 not in selectedatoms:
            continue
        value = top.constrainttype[key]
        fhout.write("%-8s %-8s %d %16f\n" %
                    (a1, a2, func,
                     value))
    fhout.write("\n")

if top.angletype != {}:
    fhout.write("[ angletypes ]\n")
    print_bondtypes(top.angletype, 3)

def print_valuestype(dic, n, removespace = False):
    def write_with_length_adjust(fh, buf, continuation = False):
        riplen = 144
        if len(buf) < riplen:
            fh.write(buf)
            if continuation:
                fh.write("\n")
            return
        else:
            pos = buf[riplen:].index(" ")
            pos += riplen
            fh.write(buf[0:pos] + " \\\n")
            write_with_length_adjust(fh, buf[pos:], True)
    for key in sorted(dic.keys()):
        selected = True
        for i in range(n):
            if key[i] not in selectedatoms and key[i] != "X":
                selected = False
                break
        if not selected:
            continue
        func = key[n]
        strformat = "%-8s"
        if removespace:
            strformat = "%s"
        for values in dic[key]:
            buf = ("%s %d %s\n" %
                   (" ".join([strformat % a for a in key[0:n]]),
                    func,
                    " ".join(values)))
            write_with_length_adjust(fhout, buf)
    fhout.write("\n")

# dihedraltypes are not scaled here. Instead, They are scaled in [ dihedrals ] section.
if top.dihedtype != {}:
    fhout.write("[ dihedraltypes ]\n")
    print_valuestype(top.dihedtype, 4)

if top.cmaptype != {}:
    fhout.write("[ cmaptypes ]\n")
    print_valuestype(top.cmaptype, 5, True)

if top.nonbondparams != {}:
    fhout.write("[ nonbond_params ]\n")
    print_nonbond2(top.nonbondparams)

# general parameter sets are done
cmap_warned = False

for molname in usedmolecules:
    fhout.write("[ moleculetype ]\n")
    fhout.write("%s\t3\n\n" % molname) # XXX: nrexcl can be different

    molindex = top.moleculetypes[molname]
    
    mol = top.molecules[molindex]
    moltop = top.moleculetopology[molname]

    scaled = molname in solutes
    scale = 1.0
    if scaled:
        scale = math.sqrt(scalefac)

    fhout.write("[ atoms ]\n")
    cgnr = 1
    nr = 1
    for (atype, aname, resnr, resname, charge, mass) in mol:
        fhout.write("%-8d %-8s %-4d %-5s %-5s %-4d %-16.5f %-16.5f" %
                    (nr, atype, resnr, resname, aname, cgnr, charge, mass))
        cgnr += 1
        nr += 1
        if scaled:
            fhout.write("%-8s %-16.5f %-16.5f\n" %
                        (atype + options.suffix, charge * scale, mass))
        else:
            fhout.write("\n")
    fhout.write("\n")

    for section in moltop:
        fhout.write("[ %s ]\n" % section)
        if scaled and section == "dihedrals":
            for l in moltop[section]:
                # scale dihedral parameters as well
                # l[0]..l[3]: ai..al
                # l[4]: func
                # l[5]..: vars
                func = int(l[4])
                if len(l) == 5:
                    # look up data from table
                    atoms = [mol[int(ai) - 1][0] for ai in l[0:4]]
                    dihed_values = lookup_dihed(atoms, func)
                else:
                    dihed_values = [l[5:]]
                for values in dihed_values:
                    # print diheds first, as they are equal anyway
                    fhout.write(" ".join(l[0:5]) + " ")
                    fhout.write(" ".join(values))
                    if func in [1, 4, 9]:
                        # Note: multB is not stated in GROMACS manual but implementation requires them
                        # func 1: Proper. phase, k, multiplicity, phaseB, kB, multB
                        # func 4: Periodic improper. phase, k, multiplicity, phaseB, kB, multB
                        # func 9: Proper mult. phase, k, multiplicity, phaseB, kB, multB
                        phaseB = float(values[0])
                        kB = float(values[1]) * scale * scale
                        multB = int(values[2])
                        fhout.write(" %16f %16f %d\n" % (phaseB, kB, multB))
                    elif func == 2:
                        # func 2: Improper. phase, k, phaseB, kB
                        phaseB = float(values[0])
                        kB = float(values[1]) * scale * scale
                        fhout.write(" %16f %16f\n" % (phaseB, kB))
                    elif func in [3, 5]:
                        # func 3: R-B dih. C1..C6, C1B..C6B
                        # func 5: Fourier dih. C1..C4, C1B..C4B
                        values = [float(c) * scale * scale for c in values]
                        fhout.write(" " + " ".join(["%16f" % v for v in values]) + "\n")
                    else:
                        error("Unsupported dihedral angle function = %d" % func)
        else:
            for l in moltop[section]:
                fhout.write(" ".join(l) + "\n")
        if section == "cmap" and not cmap_warned:
            warn("CMAP potential is not scaled")
            cmap_warned = True
        fhout.write("\n")

fhout.write("[ system ]\n")
fhout.write("REST system\n\n")
fhout.write("[ molecules ]\n")
for (s, n) in top.system:
    fhout.write("%s\t%d\n" % (s, n))

